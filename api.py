from flask import Flask, request, redirect, render_template, make_response
import rethinkdb as r
from flask_restful import Resource, Api
from validate_email import validate_email
from passlib.hash import bcrypt

app = Flask(__name__)
api = Api(app)

class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}

class login(Resource):
    def post(self):
        user = request.form['login-form-username']
        userpass = request.form['login-form-password']
        r.connect("localhost", 28015).repl()
        cursor = r.db('location').table("users").filter(r.row["username"] == user).run()
        for document in cursor:
            print(document)
            servuser = document['username']
            servpass = document['pass']
            idd = document['id']

        if user == servuser:
            if bcrypt.verify(userpass, servpass) == True:
                redirect_to_index = redirect('http://localhost:2015/blank-page.html')
                response = make_response(redirect_to_index)
                response.set_cookie('permit',value='1')
                response1 = make_response(redirect_to_index)
                response1.set_cookie('user',value=idd)
                return response
            else:
                return 'fail pass'
        else:
            return 'fail user'

class logout(Resource):
    def post(self):
        redirect_to_index = redirect('http://localhost:2015')
        response = make_response(redirect_to_index)
        response.set_cookie('permit', '', expires=0)
        return response

class register(Resource):
    def post(self):
        r.connect("localhost", 28015).repl()
        is_valid = validate_email(request.form['register-form-email'])
        email = request.form['register-form-email']
        cl_user = request.form['register-form-username']
        cursor = r.db('location').table("users").filter(r.row["email"] == email).run()
        servemail = ''
        servuser = ''
        for document in cursor:
            print(document)
            servemail = document['email']
            servuser = document['username']

        if servemail == email or servuser == cl_user:
            return 'erro email/username ja existente'
        else:
            if is_valid:
                str = request.form['register-form-username']
                if len(str) >= 6:
                    if request.form['register-form-password'] == request.form['register-form-repassword']:
                        cryppass = bcrypt.hash(request.form['register-form-password'])
                        name = request.form['register-form-username']
                        r.db('location').table('users').insert([{"email":email,"username":name,"pass":cryppass,"lat":1,"lon":1}]).run()
                        return  redirect("http://localhost:2015/counters.html", code=302)
                    else:
                        print('Erro:pass nao coinccide')
                else:
                    print('Erro:nome curto demais')
            else:
                print('Erro:email invalido')


api.add_resource(HelloWorld, '/')
api.add_resource(login, '/login_data')
api.add_resource(register, '/register_data')
api.add_resource(logout, '/logout_data')

if __name__ == '__main__':
    app.run(debug=True ,port=5440)
