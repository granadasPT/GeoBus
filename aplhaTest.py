# make sure you can connect to Google's server
import requests
import googlemaps
from flask import Flask
from flask_socketio import SocketIO, send, emit

import rethinkdb as r

r.connect("localhost", 28015).repl()

app = Flask(__name__)
socketio = SocketIO(app, async_mode="eventlet")


pos = r.db('location').table("position").filter({"id": "42fdec98-4346-4ef0-b8fe-68e2c5bbb2f1"}).run()
for document in pos:
    lat = document['lat']
    lon = document['lon']

strlat = "{:.9f}".format(lat)
strlon = "{:.9f}".format(lon)
endlat = "38.975892"
endlon = "-8.999176"
try:
  response = requests.get('http://www.google.com')
except:
  print('Can\'t connect to Google\'s server')
  raw_input('Press any key to exit.')
  quit()

# use the Google Maps API
gmaps = googlemaps.Client(key='AIzaSyArPkGYhyh-dqM0P3nB1bfa7f1VqpXR-Wk')
origins = []
destinations = []
origins.append(str(strlat) + ' ' + str(strlon))
destinations.append(str(endlat) + ' ' + str(endlon))
matrix = gmaps.distance_matrix(origins, destinations, mode="driving")
print(matrix)
