from flask import Flask
from flask_socketio import SocketIO, send, emit
import requests
import googlemaps


import rethinkdb as r
r.connect( "localhost", 28015).repl()

app = Flask(__name__)
socketio = SocketIO(app, async_mode="eventlet")

@socketio.on('data')
def handle_message(data):
    print('received json: ' + str(data['lat']) + str(data['long']))
    r.db("location").table("position").get("42fdec98-4346-4ef0-b8fe-68e2c5bbb2f1").replace(
        {"id":"42fdec98-4346-4ef0-b8fe-68e2c5bbb2f1", "lat":data['lat'], "lon":data['long']}
    ).run()

@socketio.on('bus_data')
def handle_message(bus_data):
    coiso = r.db('location').table("position").get('42fdec98-4346-4ef0-b8fe-68e2c5bbb2f1').run()
    print(coiso)
    emit('response', coiso)

@socketio.on('route')
def handle_message(route):
    start = r.db('location').table("stops").get('36da96b6-7e5d-4405-8ecc-ba680cbd946d').run()
    end = r.db('location').table("stops").get('c441abe9-6750-4c89-a680-769735222998').run()
    print(start)
    print(end)
    emit('start_route_response', start)
    emit('end_route_response', end)



@socketio.on('matrix')
def handle_message(matrix):
    bus_pos = r.db('location').table("position").get("42fdec98-4346-4ef0-b8fe-68e2c5bbb2f1").run()
    strlat = bus_pos['lat']
    strlon = bus_pos['lon']
    final_stop = r.db('location').table('stops').get("c441abe9-6750-4c89-a680-769735222998").run()
    endlat = final_stop['lat']
    endlon = final_stop['lon']
    try:
      response = requests.get('http://www.google.com')
    except:
      print('Can\'t connect to Google\'s server')
      raw_input('Press any key to exit.')
      quit()
  # use the Google Maps API
    gmaps = googlemaps.Client(key='AIzaSyArPkGYhyh-dqM0P3nB1bfa7f1VqpXR-Wk')
    origins = []
    destinations = []
    origins.append(str(strlat) + ' ' + str(strlon))
    destinations.append(str(endlat) + ' ' + str(endlon))
    matrix = gmaps.distance_matrix(origins, destinations, mode="driving")
    print(matrix)
    emit('matrix_response', matrix)



if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0')
else:
    print('Error')
